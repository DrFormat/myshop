from django.conf import settings
from django.contrib import admin
from django.urls import path
from django.conf.urls import include, url
from django.conf.urls.static import static

urlpatterns = [
    url(r'^auth/', include('rest_framework.urls')),
    path('app/', include('app.urls')),
    path('admin/', admin.site.urls),
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

