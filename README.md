# README #

 Сервис службы доставки товаров.

### Технологии ###

* Python 3
* Django
* Django Rest Framework
* PostgreSQL

### Получение исходников и подготовка окружения ###

Получим исходники

    git clone https://DrFormat@bitbucket.org/DrFormat/myshop.git /usr/src/myshop
    git checkout develop

Создаем виртуальное окружение

    pyvenv /usr/src/myshop
    cd /usr/src/myshop
    . bin/activate

Неоходимые пакеты указаны в файле requirements.txt

    pip3 install -r requirements.txt

### Запуск ###
Для настройки БД создадим файл myshop/settings_local.py следующего содержания

    DATABASES = {
        'default': {
            'ENGINE': 'django.db.backends.postgresql_psycopg2',
            'NAME': 'myshop',
            'USER': 'myshop',
            'PASSWORD': '',
            'HOST': '',
            'PORT': '',
        }
    }
Это позволит обновлять исходники из репы не меняя конфиг БД

Запустим миграцию БД и создадим суперпользователя

    python3 manage.py makemigrations
    python3 manage.py migrate
    python3 manage.py createsuperuser

Запустим проект

    python3 manage.py runserver
