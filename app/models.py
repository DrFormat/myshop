from django.db import models
from django.contrib.auth.models import User


DELIVERY_STATUS_CHOICES = (
    (1, 'Ожидает вручения'),
    (2, 'Получено'),
)

REFUSAL_STATUS_CHOICES = (
    (1, 'Отказ'),
    (2, 'Превышен срок хранения'),
)


class Profile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    address = models.CharField(verbose_name='Адрес', max_length=250)
    image = models.ImageField(verbose_name='Фото', upload_to='users')

    def __str__(self):
        return self.user.username

    class Meta:
        verbose_name = 'Пользователь'
        verbose_name_plural = 'Пользователи'


class Base(models.Model):
    date_created = models.DateTimeField(auto_now_add=True)
    date_modified = models.DateTimeField(auto_now_add=True)

    class Meta:
        abstract = True


class Product(Base):
    article = models.CharField(verbose_name='Артикул', max_length=250, unique=True)
    manufactured = models.CharField(verbose_name='Производитель', max_length=250)
    name = models.CharField(verbose_name='Наименование', max_length=250)
    barcode = models.CharField(verbose_name='ШК', max_length=250, unique=True)
    image = models.ImageField(verbose_name='Фото', upload_to='products')
    price = models.FloatField(verbose_name='Цена/шт')
    weight = models.FloatField(verbose_name='Вес')

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = 'Товар'
        verbose_name_plural = 'Товары'


class Parcel(Base):
    goods = models.ManyToManyField(Product)
    recipient = models.ForeignKey(User, on_delete=models.CASCADE, verbose_name='Получатель')
    shipment_date = models.DateTimeField(verbose_name='Дата отправки', null=True, blank=True)
    delivery_status = models.PositiveSmallIntegerField(verbose_name='Признак вручения', choices=DELIVERY_STATUS_CHOICES, db_index=True, null=True, blank=True)
    delivery_date = models.DateTimeField(verbose_name='Дата вручения', null=True, blank=True)
    delivery_cost = models.FloatField(verbose_name='Стоимость доставки', )
    refusal_status = models.PositiveSmallIntegerField(verbose_name='Признак отказа', choices=REFUSAL_STATUS_CHOICES, db_index=True, null=True, blank=True)

    class Meta:
        verbose_name = 'Посылка'
        verbose_name_plural = 'Посылки'
