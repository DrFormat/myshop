from django.conf.urls import url
from .views import UserList, UserDetail, UserStats, ProductList, ProductDetail, ProductStats, ParcelList, ParcelDetail

urlpatterns = [
    url(r'^users/$', UserList.as_view(), name='user-list'),
    url(r'^users/(?P<pk>\d+)/$', UserDetail.as_view(), name='user-detail'),
    url(r'^users/(?P<pk>\d+)/stats/$', UserStats.as_view(), name='user-stats'),
    url(r'^products/$', ProductList.as_view(), name='product-list'),
    url(r'^products/(?P<pk>\d+)/$', ProductDetail.as_view(), name='product-detail'),
    url(r'^products/(?P<pk>\d+)/stats/$', ProductStats.as_view(), name='product-stats'),
    url(r'^parcels/$', ParcelList.as_view(), name='parcel-list'),
    url(r'^parcels/(?P<pk>\d+)/$', ParcelDetail.as_view(), name='parcel-detail'),
]
