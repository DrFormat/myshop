from rest_framework import serializers
from django.db.models import Sum
from django.contrib.auth.models import User
from .models import Profile, Product, Parcel


class ProfileSerializer(serializers.ModelSerializer):
    class Meta:
        model = Profile
        fields = ('image', 'address')


class UserSerializer(serializers.ModelSerializer):
    profile = ProfileSerializer(many=False, read_only=False)

    class Meta:
        model = User
        fields = ('first_name', 'last_name', 'username', 'profile')

    def create(self, validated_data):
        profile_data = validated_data.pop('profile')
        user = User.objects.create(**validated_data)
        Profile.objects.create(user=user, **profile_data)
        return user

    def update(self, instance, validated_data):
        profile_data = validated_data.pop('profile')
        profile = instance.profile
        instance.first_name = validated_data.get('first_name', instance.first_name)
        instance.last_name = validated_data.get('last_name', instance.last_name)
        instance.username = validated_data.get('username', instance.username)
        instance.save()
        profile.address = profile_data.get('address', profile.address)
        profile.image = profile_data.get('image', profile.image)
        profile.save()
        return instance


class ProductSerializer(serializers.ModelSerializer):
    class Meta:
        model = Product
        fields = ('article', 'name', 'barcode', 'image', 'price', 'weight')


class ProductStatsSerializer(serializers.Serializer):
    refusal_percent = serializers.IntegerField()
    id__count = serializers.IntegerField()
    refusal_status__count = serializers.IntegerField()


class ParcelSerializer(serializers.ModelSerializer):
    class Meta:
        model = Parcel
        fields = ('goods', 'recipient', 'shipment_date', 'delivery_status', 'delivery_date', 'delivery_cost', 'refusal_status')
        read_only_fields = ('delivery_cost',)

    def validate(self, data):
        print(data)
        goods_in = Product.objects.filter(id__in={x.id for x in data['goods']}).aggregate(Sum('price'), Sum('weight'))
        if goods_in['weight__sum'] > 100 or goods_in['weight__sum'] < 0.1:
            raise serializers.ValidationError("Вес посылки должен быть меньше 100 кг, но больше 100 г.")
        if goods_in['price__sum'] > 1000:
            raise serializers.ValidationError("Стоимость посылки не должна превышать 1000 евро.")
        if data['delivery_status'] and data['refusal_status']:
            raise serializers.ValidationError("Признак вручения не может быть установлен одновременно с признаком отказа.")
        if not data['shipment_date'] and data['delivery_date']:
            raise serializers.ValidationError("Посылка не была отправлена")
        if data['shipment_date'] and data['delivery_date']:
            if data['shipment_date'] > data['delivery_date']:
                raise serializers.ValidationError("Дата вручения не должна предшествовать дате отправки.")
        data['delivery_cost'] = goods_in['price__sum'] / 100 * 13
        return data
