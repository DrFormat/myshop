from django.http import Http404
from rest_framework.generics import ListCreateAPIView, RetrieveUpdateDestroyAPIView, RetrieveAPIView
from rest_framework.response import Response
from django.contrib.auth.models import User
from django.db.models import Count
from app.models import Product, Parcel
from .serializers import UserSerializer, ProductSerializer, ProductStatsSerializer, ParcelSerializer


class UserList(ListCreateAPIView):
    """Список пользователей"""
    model = User
    queryset = model.objects.all()
    serializer_class = UserSerializer


class UserDetail(RetrieveUpdateDestroyAPIView):
    """Детальная информация о пользователе"""
    model = User
    queryset = model.objects.all()
    serializer_class = UserSerializer


class UserStats(RetrieveAPIView):
    """Статистика пользователя. Процент отказа от посылок"""
    model = User
    queryset = model.objects.all()
    serializer_class = ProductStatsSerializer

    def get(self, request, *args, **kwargs):
        print(kwargs.get('pk'))
        goods = {}
        goods_refusal = Parcel.objects.filter(recipient__id=kwargs.get('pk')).aggregate(Count('id'), Count('refusal_status'))
        if goods_refusal['id__count'] == 0:
            raise Http404()
        goods['refusal_percent'] = goods_refusal['refusal_status__count'] / goods_refusal['id__count'] * 100
        print(goods)
        return Response(goods)


class ProductList(ListCreateAPIView):
    """Список товаров"""
    model = Product
    queryset = model.objects.all()
    serializer_class = ProductSerializer


class ProductDetail(RetrieveUpdateDestroyAPIView):
    """Детальная информация о товаре"""
    model = Product
    queryset = model.objects.all()
    serializer_class = ProductSerializer


class ProductStats(RetrieveAPIView):
    """Статистика товара. Процент отказа и топ сопутствующих товаров"""
    model = Product
    queryset = model.objects.all()
    serializer_class = ProductStatsSerializer

    def get(self, request, *args, **kwargs):
        goods = {}
        goods_refusal = Parcel.objects.filter(goods__id=kwargs.get('pk')).aggregate(Count('id'), Count('refusal_status'))
        goods_with_this = Product.objects.filter(parcel__goods__in=kwargs.get('pk'))\
            .values('id', 'name')\
            .annotate(parcels=Count('parcel__id'))\
            .order_by('-parcels')[:4]
        goods_with_this = list(filter(lambda x: x['id'] != int(kwargs.get('pk')), goods_with_this))
        goods['refusal_percent'] = goods_refusal['refusal_status__count'] / goods_refusal['id__count'] * 100
        goods['goods_with_this'] = goods_with_this
        return Response(goods)


class ParcelList(ListCreateAPIView):
    """Список посылок"""
    model = Parcel
    queryset = model.objects.all()
    serializer_class = ParcelSerializer


class ParcelDetail(RetrieveUpdateDestroyAPIView):
    """Детальная информация о посылке"""
    model = Parcel
    queryset = model.objects.all()
    serializer_class = ParcelSerializer
