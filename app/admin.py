from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from django.contrib.auth.models import User
from .models import Profile, Product, Parcel


class UserInline(admin.StackedInline):
    model = Profile
    can_delete = False
    verbose_name_plural = 'Доп. информация'


class CustomUserAdmin(UserAdmin):
    list_display = ('id', 'username', 'email', 'last_name', 'first_name', 'is_staff')
    inlines = (UserInline,)
    ordering = ('id',)


class ProductAdmin(admin.ModelAdmin):
    list_display = ['article', 'name', 'barcode', 'image', 'price', 'weight']
    list_editable = ['name', 'barcode', 'image', 'price', 'weight']
    ordering = ('id',)


class ParcelAdmin(admin.ModelAdmin):
    list_display = ['recipient', 'shipment_date', 'delivery_status', 'refusal_status', 'delivery_date', 'delivery_cost']
    list_editable = ['delivery_status', 'refusal_status', 'delivery_cost']
    ordering = ('id',)


admin.site.site_header = 'myshop'
admin.site.unregister(User)
admin.site.register(User, CustomUserAdmin)
admin.site.register(Product, ProductAdmin)
admin.site.register(Parcel, ParcelAdmin)
